import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList, StatusBar } from 'react-native';



export default class App extends React.Component {
    render() {
        return (

            <View style={styles.container}>
                <StatusBar
                    backgroundColor={"white"}
                    translucent={false} />
                <View style={styles.header}>
                    <Image source={require('./images/logo.png')} style={styles.logo} />
                </View>
                <View style={styles.title}><Text style={{ fontSize: 24}}>Register</Text></View>
                <View style={styles.content}>
                    <Text style={styles.textKeterangan}>Username</Text>
                    <View style={styles.textBox}></View>
                    <Text style={styles.textKeterangan}>Email</Text>
                    <View style={styles.textBox}></View>
                    <Text style={styles.textKeterangan}>Password</Text>
                    <View style={styles.textBox}></View>
                    <Text style={styles.textKeterangan}>Ulangi Password</Text>
                    <View style={styles.textBox}></View>
                    <View style={styles.buttonArr}>
                        <View style={styles.button1}>
                            <Text style={styles.text}>Daftar</Text>
                        </View>
                    </View>
                    <View style={styles.buttonArr}>
                        <Text style={{ fontSize: 16, marginTop: 10, marginBottom: 10 }}>Atau</Text>
                    </View>
                    <View style={styles.buttonArr}>
                        <View style={styles.button2}>
                            <Text style={styles.text}>Masuk ?</Text>
                        </View>
                    </View>
                </View>                
            </View>
        )
    }
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    header: {
        marginTop: 50,
        height: 120,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    logo: { 
        width: 360, 
        height: 55, 
        marginLeft: 50 
    },
    title: {
        marginTop: 50,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 55
    },
    content: {
        height: 600,
        alignItems: 'center',
        padding: 20
    },
    textKeterangan:{ 
        fontSize:16,
        textAlign: "left" 
    },
    textBox: {
        width: 300,
        height: 40,
        marginLeft: 25,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'grey',
        marginBottom: 20
    },
    button1: {        
        width: 110,
        height: 50,
        borderRadius: 20,
        backgroundColor: '#3EC6FF'
    },
    button2: {        
        width: 110,
        height: 50,
        borderRadius: 20,
        backgroundColor: '#003366'
    },
    buttonArr: {
        width: 300,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        textAlign: 'center',
        marginTop: 8,
        color: 'white',
        fontWeight: 'bold',
        fontSize: 24
    }
});