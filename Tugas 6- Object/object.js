//soal 1
{
    console.log("==jawaban 1==")

    function arrayToObject(arr) {
        var now = new Date()
        var thisYear = now.getFullYear()
        var peopleObj = {}

        if (arr.length == 0) {
            console.log('""');
        }
        else {
            for (i = 0; i < arr.length; i++) {
                peopleObj[(i + 1) + ". " + arr[i][0] + ' ' + arr[i][1]] = {
                    firstName: arr[i][0],
                    lastName: arr[i][1],
                    gender: arr[i][2],
                    age: (arr[i][3] == null || arr[i][3] > thisYear) ? "Invalid Birth Year" : thisYear - arr[i][3]

                }
            }
            console.log(peopleObj);

        }
    }

    // Driver Code
    var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
    arrayToObject(people)
    /*
        1. Bruce Banner: { 
            firstName: "Bruce",
            lastName: "Banner",
            gender: "male",
            age: 45
        }
        2. Natasha Romanoff: { 
            firstName: "Natasha",
            lastName: "Romanoff",
            gender: "female".
            age: "Invalid Birth Year"
        }
    */

    var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
    arrayToObject(people2)
    /*
        1. Tony Stark: { 
            firstName: "Tony",
            lastName: "Stark",
            gender: "male",
            age: 40
        }
        2. Pepper Pots: { 
            firstName: "Pepper",
            lastName: "Pots",
            gender: "female".
            age: "Invalid Birth Year"
        }
    */

    // Error case 
    arrayToObject([]) // ""
}
//soal 2
{
    console.log("==jawaban 2==")

    function shoppingTime(memberId, money) {
        var sale = {
            "Sepatu Stacattu": 1500000,
            "Baju Zoro": 500000,
            "Baju H&N": 250000,
            "Sweater Uniklooh": 175000,
            "Casing Handphone": 50000,

        };

        if (memberId == null || memberId === '') {
            return "Mohon maaf, toko X hanya berlaku untuk member saja";
        }
        else if (money < 50000) {
            return "Mohon maaf, uang tidak cukup"
        }
        else {
            var harga = Object.values(sale).sort();
            var listPurchased = [];
            var jumlah = 0;

            for (i = 0; i < harga.length; i++) {
                if (harga[i] < money) {
                    jumlah += harga[i];
                    listPurchased.push(Object.keys(sale)[Object.values(sale).indexOf(harga[i])]);
                }
            }
            var hasil = {
                memberId: memberId,
                money: money,
                listPurchased: listPurchased,
                changeMoney: money - jumlah,
            };

            return hasil;
        }
    }

    // TEST CASES
    console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
    console.log(shoppingTime('82Ku8Ma742', 170000));
    //{ memberId: '82Ku8Ma742',
    // money: 170000,
    // listPurchased:
    //  [ 'Casing Handphone' ],
    // changeMoney: 120000 }
    console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
    console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
    console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


}
//soal 3
{
    console.log("==jawaban 3==")

    function naikAngkot(arrPenumpang) {
        rute = ['A', 'B', 'C', 'D', 'E', 'F'];

        var hasil = [];



        if (arrPenumpang != null) {
            for (i = 0; i < arrPenumpang.length; i++) {
                var objAngkot = {};
                objAngkot.penumpang = arrPenumpang[i][0];
                objAngkot.asal = arrPenumpang[i][1];
                objAngkot.turun = arrPenumpang[i][2];
                objAngkot.ongkos = (rute.indexOf(objAngkot.turun) - rute.indexOf(objAngkot.asal)) * 2000;
                hasil.push(objAngkot);
            }
        }
        else {
            return arrPenumpang;
        }

        return hasil;
    }

    //TEST CASE
    console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
    // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
    //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

    console.log(naikAngkot([])); //[]
}