//soal 1
{
    console.log("==jawaban 1==")
    function range(a, b) {
        var deret = [];
        if (a < b) {
            for (i = a; i <= b; i++) {
                deret.push(i);
            }
        }
        else if (a > b) {
            for (i = a; i >= b; i--) {
                deret.push(i);
            }
        }
        else if (a == null || b == null) {
            deret = -1;
        }
        return deret;

    }

    console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    console.log(range(1)) // -1
    console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
    console.log(range(54, 50)) // [54, 53, 52, 51, 50]
    console.log(range()) // -1 
}
//soal 2
{
    console.log("==jawaban 2==")
    function rangeWithStep(c, d, e) {
        var deret2 = []
        if (c < d) {
            for (i = c; i <= d; i += e) {
                deret2.push(i);
            }
        }
        else if (c > d) {
            for (i = c; i >= d; i -= e) {
                deret2.push(i);
                deret2.sort(function (value1, value2) { return value2 - value1 });
            }
        }
        return deret2;

    }

    console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
    console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
    console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
    console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
}
//soal 3
{
    console.log("==jawaban 3==")
    function sum(awal = -1, akhir = -1, jarak = 1) {
        while (awal == -1 && akhir == -1) {
            return 0;
        }
        while (awal != -1 && akhir == -1) {
            return awal;
        }
        while (awal != -1 && akhir != -1) {
            var jumlah = 0;
            if (awal < akhir) {
                for (i = awal; i <= akhir; i++) {
                    jumlah += i;
                    i += jarak - 1;
                }
            } else {
                for (i = awal; i >= akhir; i--) {
                    jumlah += i;
                    i - +jarak - 1;
                }
            }
            return jumlah;
        }


    }

    console.log(sum(1, 10)) // 55
    console.log(sum(5, 50, 2)) // 621
    console.log(sum(15, 10)) // 75
    console.log(sum(20, 10, 2)) // 90
    console.log(sum(1)) // 1
    console.log(sum()) // 0 
}
//soal 4
{
    console.log("==jawaban 4==")
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]

    function dataHandling(input) {
        var hasil = "";
        for (i = 0; i < input.length; i++) {
            hasil += "Nomor ID: " + input[i][0] +
                "\nNama Lengkap: " + input[i][1] +
                "\nTTL:  " + input[i][2] + " " + input[i][3] +
                "\nHobi:  " + input[i][4] +
                "\n\n";
        }
        return hasil;
    }
    console.log(dataHandling(input));


}
//soal 5
{
    console.log("==jawaban 5==")
    function balikKata(str) {
        var kata = str;
        var balik = '';
        for (let i = str.length - 1; i >= 0; i--) {
            balik = balik + kata[i];
        }

        return balik;
    }
    console.log(balikKata("Kasur Rusak")) // kasuR rusaK
    console.log(balikKata("SanberCode")) // edoCrebnaS
    console.log(balikKata("Haji Ijah")) // hajI ijaH
    console.log(balikKata("racecar")) // racecar
    console.log(balikKata("I am Sanbers")) // srebnaS ma I 
}
//soal 6
{
    console.log("==jawaban 6==");

    var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

    function dataHandling2(input2) {
        var nama = input2.slice(1, 2) + "Elsharawy";
        var provinsi = "Provinsi" + input2.slice(2, 3);
        var gender = "Pria";
        var sekolah = "SMA Internasional Metro";
        var tanggal = input2.slice(3, 4)[0].split("/");
        var bulan = "";

        switch (tanggal[1]) {
            case "01": {
                bulan = "Januari";
                break;
            }
            case "02": {
                bulan = " Februari ";
                break;
            }
            case "03": {
                bulan = " Maret ";
                break;
            }
            case "04": {
                bulan = " April ";
                break;
            }
            case "05": {
                bulan = " Mei ";
                break;
            }
            case "06": {
                bulan = " Juni ";
                break;
            }
            case "07": {
                bulan = " Juli ";
                break;
            }
            case "08": {
                bulan = " Agustus ";
                break;
            }
            case "09": {
                bulan = " September ";
                break;
            }
            case "10": {
                bulan = " Oktober ";
                break;
            }
            case "11": {
                bulan = " November ";
                break;
            }
            case "12": {
                bulan = " Desember ";
                break;
            }
            default: {
                bulan = " bulan tidak diketahui ";
            }
        }


        input2.splice(1, 2, nama, provinsi);
        input2.splice(4, 1, gender, sekolah);
        console.log(input2);

        console.log(bulan);

        console.log(tanggal.slice(0).sort(function (value1, value2) { return value2 - value1; }));

        console.log(tanggal.join("-"));

        console.log(input2.slice(1, 2)[0].slice(0, 14));
    }

    dataHandling2(input2);
}
