var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
var waktu = 10000;
readBooksPromise(waktu, books[0]).then(function (a) { readBooksPromise(a, books[1]).then(function (a) { readBooksPromise(a, books[2]) }) })