import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class skill extends Component {
    render() {
        let skill = this.props.skill;
        return (
            <View style={styles.container}>
                <Icon name={skill.iconName} size={50} />
                <View style={{ marginLeft: 10 }}>
                    <Text style={{ textAlign: 'left', fontWeight: 'bold', color: '#0b396a', width: 180, height: 30 }}>{skill.skillName}</Text>
                    <Text style={{ textAlign: 'left', fontWeight: 'bold', color: '#0b396a', width: 180, height: 30 }}>{skill.categoryName}</Text>
                    <Text style={{ textAlign: 'right', fontWeight: 'bold', color: 'white', width: 180, height: 35, fontSize: 30 }}>{skill.percentageProgress}</Text>
                </View>
                <View>
                    <Icon name="chevron-right" size={50} />
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        padding: 15,
        backgroundColor: '#B4E9FF',
        borderRadius: 10,
        flexDirection: 'row',
    }

});
