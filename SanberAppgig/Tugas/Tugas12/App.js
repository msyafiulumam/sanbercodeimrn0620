import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require('./images/yt_logo_rgb_light.png')} style={{width:100, height:22}}/>
                    <View style={styles.rightNav}>
                        <Icon name="search" size={25}/>
                    </View>
                </View>
                
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1

    },
    navBar:{
        height: 75,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
});