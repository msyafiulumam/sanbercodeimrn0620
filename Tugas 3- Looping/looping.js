//soal 1
{
    console.log("==jawaban 1==")

    var flag1 = 2;
    var flag2 = 20;
    console.log("LOOPING PERTAMA");
    while (flag1 <= 20) {
        console.log(flag1 + ' - I love coding');
        flag1 += 2;
    }
    console.log("LOOPING KEDUA");
    while (flag2 > 1) {
        console.log(flag2 + ' - I will become a mobile developer');
        flag2 -= 2;
    }
}
//soal 2
{
    console.log("==jawaban 2==")

    for (var angka = 1; angka <= 20; angka++) {
        if (angka % 2 == 1 && angka % 3 == 0) {
            console.log("I Love Coding")
        }
        else if (angka % 2 == 0) {
            console.log("Berkualitas")
        }
        else if (angka % 2 == 1) {
            console.log("Santai");
        }
    }
}
//soal 3
{
    console.log("==jawaban 3==");

    var a = "";

    for (i = 1; i <= 4; i++) {
        for (j = 1; j <= 8; j++) {
            a += "#"
        }
        console.log(a);
        a = ""
        j = 1;
    }
}
//soal 4
{
    console.log("==jawaban 4==");

    var b = ""
    for (i = 1; i <= 7; i++) {
        for (j = i; j >= 1; j--) {
            b += "#"
        }
        console.log(b);
        b = ""
        j = 1;
    }
}
//soal 5
{
    console.log("==jawaban 5==");

    var c = ""
    for (i = 1; i <= 8; i++) {
        for (j = 1; j <= 4; j++) {
            if (i % 2 == 1) {
                c += " #"
            }
            else if (i % 2 == 0) {
                c += "# "
            }

        }
        console.log(c)

        c = ""
        j = 1

    }
}