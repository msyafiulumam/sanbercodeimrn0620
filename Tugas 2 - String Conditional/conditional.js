//soal 1
{
    console.log("==Jawaban 1==")
    var nama = "umam";
    var peran = "werewolf";

    if (nama == "umam") {
        if (peran == "") {
            console.log("Peran harus diisi!")
        }
        else if (peran == "guard") {
            console.log("Halo guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf")
        }
        else if (peran == "penyihir") {
            console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
        }
        else if (peran == "werewolf") {
            console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
        }
        else {
            console.log("Peran tersebut tidak ada!")
        }

    }
    else if (nama == '') {
        console.log("Nama harus diisi!")
    }
}
//soal 2
{
    console.log("==Jawaban 2==")
    var tanggal = 6;
    var bulan = 8;
    var tahun = 1998;

    switch(bulan){
        case 1: {console.log(tanggal+" januari "+tahun); break;}
        case 2: {console.log(tanggal+" februari "+tahun); break;}
        case 3: {console.log(tanggal+" maret "+tahun); break;}
        case 4: {console.log(tanggal+" april "+tahun); break;}
        case 5: {console.log(tanggal+" mei "+tahun); break;}
        case 6: {console.log(tanggal+" juni "+tahun); break;}
        case 7: {console.log(tanggal+" juli "+tahun); break;}
        case 8: {console.log(tanggal+" agustus "+tahun); break;}
        case 9: {console.log(tanggal+" september "+tahun); break;}
        case 10: {console.log(tanggal+" oktober "+tahun); break;}
        case 11: {console.log(tanggal+" november "+tahun); break;}
        case 12: {console.log(tanggal+" desember "+tahun); break;}
        default: {console.log("tanggal tidak ditemukan!"); break;}
    }


}